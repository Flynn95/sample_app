require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
    test "invalid signup check" do
        get signup_path
        assert_no_difference 'User.count' do
            post users_path, params:    {   user: { name: "",
                                                    email: "user@invalid",
                                                    password: "bar",
                                                    password_confirmation: "foo"
                                                    }
                                        }
        end
        assert_template "users/new"
    end

    test "valid signup check" do
        get signup_path
        assert_difference 'User.count', 1 do
            post users_path, params:    { user:   { name: "Flynn",
                                                    email: "flynn.ryder95@gmail.com",
                                                    password: "foobar",
                                                    password_confirmation: "foobar"
                                                    }
                                        }
        end
        follow_redirect!
        # assert_template "users/show"
    end
end
