require 'test_helper'

class UserTest < ActiveSupport::TestCase
    def setup
      @user = User.new(name: 'Example user', email: 'mail@example.com',
                        password: 'foobar', password_confirmation: 'foobar',
                        activated: true)
    end

    test 'should be valid' do
      assert @user.valid?
    end

    test 'email addresses should be saved as lower-case' do
      mixed_case_email = 'Foo@ExAMPle.CoM'
      @user.email = mixed_case_email
      @user.save
      assert_equal mixed_case_email.downcase, @user.reload.email
    end

    test 'password should be present (non blank)' do
      @user.password = @user.password_confirmation = ' ' * 6
      assert_not @user.valid?
    end

    test 'password should not be too short (minimum length)' do
      @user.password = @user.password_confirmation = ' ' * 5
      assert_not @user.valid?
    end

    test 'authenticated? should return false for a user with nil digest' do
      assert_not @user.authenticated?(:remember, '')
    end

    test 'associated microposts should be destroyed along with user' do
      @user.save
      @user.microposts.create!(content: 'Lorem ipsum')
      assert_difference 'Micropost.count', -1 do
        @user.destroy
      end
    end

    test 'should follow and unfollow a user' do
      flynn = users(:flynn)
      ryder = users(:flynn_ryder)
      assert_not flynn.following?(ryder)
      flynn.follow(ryder)
      assert flynn.following?(ryder)
      assert ryder.followers.include?(flynn)
      flynn.unfollow(ryder)
      assert_not flynn.following?(ryder)
    end
end
